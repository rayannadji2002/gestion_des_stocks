package MonPackage;

public class Article {

	public static void main(String[] args) 
	{

	} 
	 private int ref ;
	 private String nom ;
	 private int prix ;
	 private int quantite ;
	 
	 public Article(int refBis , String nomBis , int prixBis , int quantiteBis) 
	 {
		 ref = refBis ; 
		 nom = nomBis ;
		 prix = prixBis ;
		 quantite = quantiteBis ;
	 }

	public int getRef() {
		return ref;
	}

	public void setRef(int ref) {
		this.ref = ref;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	 
	 public Article() 
	 {
		 ref = Saisie.lire_int("Quelle est la ref du produit ") ;
		 nom = Saisie.lire_String("Quelle est le nom du produit ") ;
		 prix = Saisie.lire_int("Quelle est le prix du produit ") ;
		 quantite = Saisie.lire_int("Quelle est la quantite du produit ") ;
	 }
	 
	 public void Affiche() 
	 {
		 System.out.println("Voici la ref de votre article " + ref );
		 System.out.println("Voici le nom de votre article " + nom );
		 System.out.println("Voici le prix de votre article " + prix );
		 System.out.println("Voici quantite de votre article " + quantite );
		 
	 }


}
