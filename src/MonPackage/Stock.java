package MonPackage;

import java.util.ArrayList;

public class Stock 
{
	//Déclaration de l'attribut Collection
	private ArrayList<Article> Collection ; 
	
	
	//Constructeur
	
	public Stock() 
	{
        Collection = new ArrayList<Article>();
    }
	
	public void afficheCompte() 
	{
		
		for(int i = 0 ; i < Collection.size() ; i ++) 
		{
			Article unarticle = Collection.get(i); //Recuperation de l'article qu'on stock dans une variable Article
			unarticle.Affiche(); //Affichage de l'article grace a la methode Affiche() de la class Article
		}
		
	}
	public void Ajout() 
	{
		 int Ajouter ;
		 Ajouter = Saisie.lire_int("Combien d'article voulez-vous ajoutez ") ;
		 for(int i = 0 ; i < Ajouter ; i++) 
		 {
			 Article article = new Article(); //creation du nouvel objet
			 Collection.add(article); // ajout  de l'objet dans la collection
			 System.out.println("Votre article a bien été ajouter"); 
		 }
	}
	
	public void Recherche() 
	{
		int NouvelRef ;
		boolean reponse = false ;
		NouvelRef = Saisie.lire_int("Quelle référence recherchez vous ? ") ;
		for(int i = 0 ; i < Collection.size() ; i ++) 
		{
			if (Collection.get(i).getRef() == NouvelRef )
			{
				Collection.get(i).Affiche();
				System.out.println("Tient ton article sal chien");
				reponse = true ;
			} 	
			
		}
		if (reponse == false ) 
		{
			System.out.println("Y'a pas ton truc la");

		}
		
	}
	
	public void RechercheNom() 
	{
		String NouveauNom ;
		boolean reponse = false ;
		NouveauNom = Saisie.lire_String("Quelle est le nom du produit que vous cherchez ? ") ;
		for(int i = 0 ; i < Collection.size() ; i ++) 
		{
			if (Collection.get(i).getNom().equals(NouveauNom))
			{
				Collection.get(i).Affiche();
				System.out.println("Tient ton article sal chien");
				reponse = true ;
			} 	
			
		}
		if (reponse == false ) 
		{
			System.out.println("Y'a pas ton truc la");

		}
		
	}
	
	
	public void Interva() 
	{
		int Prixmin ;
		int Prixmax ;
		boolean reponse = false ;


		Prixmin = Saisie.lire_int("Votre prix au minimum" ) ;
		Prixmax = Saisie.lire_int("Votre prix au maximum" ) ;
		System.out.println("Resultat de la recherche");
		for(int i = 0 ; i < Collection.size() ; i ++) 
			
		{
			if(Collection.get(i).getPrix()>= Prixmin && Collection.get(i).getPrix() <= Prixmax ) 
			
			{
				Collection.get(i).Affiche();
				reponse = true ;

				
			}

		}
		if (reponse == false ) 
		{
			System.out.println("Y'a pas d'article a ce prix la clochard");

		}


	}
	
	public void Supp() 
	{
		String NouveauNom ;
		boolean reponse = false ;
		NouveauNom = Saisie.lire_String("Quelle est le nom du produit que vous voulez supp? ") ;
		for(int i = 0 ; i < Collection.size() ; i++) 
		{
			if(Collection.get(i).getNom().equals(NouveauNom)) 
			{
				Collection.remove(Collection.get(i));
				reponse = true ;
				System.out.println("L'article a bien été supp");

				
			}	
	}
		if(reponse==false) 
		{
			System.out.println("L'article n'a pas pu etre supp");

		}

	}
	
	
	public void ModifArticle() 
	{
		int NouvelRef ;
		boolean reponse = false ;
		String Modif ;
		NouvelRef = Saisie.lire_int("Quelle référence recherchez vous ? ") ;
		for(int i = 0 ; i < Collection.size() ; i ++) 
		{
			if (Collection.get(i).getRef() == NouvelRef )
			{
				Modif =  Saisie.lire_String("Que voulez vous modifiez? ") ;
				Modif.toLowerCase();
				if(Modif.equals("nom")) 
				{
					String unNom ;
					unNom = Saisie.lire_String("Modifier le Nom ") ;
					Collection.get(i).setNom(unNom) ; 
					System.out.println("Vos modification on bien été enregistre");
					reponse = true ;
				}
				else if(Modif.equals("prix"))
				{
					int unPrix ;
					unPrix = Saisie.lire_int("Modifier le Prix ") ;
					Collection.get(i).setPrix(unPrix) ;
					System.out.println("Vos modification on bien été enregistre");
					reponse = true ;

					
				}
				else if (Modif.equals("quantite")) 
				{
					int uneQuantite ;
					uneQuantite = Saisie.lire_int("Modifier la quantité ") ;
					Collection.get(i).setQuantite(uneQuantite) ; 
					System.out.println("Vos modification on bien été enregistre");
					reponse = true ;

					
				}
				else
				{
					System.out.println("Nous n'avons pas pu faire vos modification");

				}
				
				
			}
		

			} 
		if(reponse==false )
		{
			System.out.println("Il n'y pas l'article rechercher");

		}
		
		
	
}
	
	public void AjoutArticle()
	{
		Article UnNouveauArticle = new Article();
		Collection.add(UnNouveauArticle) ; 
	}

}
 

